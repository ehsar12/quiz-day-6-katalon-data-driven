<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_AddRemoveCart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>d7c3873f-5276-421e-b60e-c0e43090c69d</testSuiteGuid>
   <testCaseLink>
      <guid>f6d5f713-1431-4472-89c2-3daae99a003c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open_Browser</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>57f74e59-6768-4327-9693-f6a7cbe73fd9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>01ca6289-c8b7-4d58-b4eb-2367adace3f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC_Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>76b1789b-a120-4cf4-8242-b306f9bf6cf6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Raw Data/Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>76b1789b-a120-4cf4-8242-b306f9bf6cf6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ab6b6e18-0195-4d02-9312-18e47646bc3c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>76b1789b-a120-4cf4-8242-b306f9bf6cf6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>04a6148e-14cf-40e9-8667-1d91024303eb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7fadc0e7-c7ec-4d97-a0fc-4bd20a62a50a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product/TC_AddToCart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>98809dd3-c1c5-4878-a490-792232386d5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product/TC_RemoveFromCart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>46bae847-294a-4160-bddc-12702b204973</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Close_Browser</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
