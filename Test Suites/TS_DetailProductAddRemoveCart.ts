<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_DetailProductAddRemoveCart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>193da3c0-044d-41da-9727-18e18ec0eebb</testSuiteGuid>
   <testCaseLink>
      <guid>34024600-3a8b-4fde-b1db-4dd4179e8cbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open_Browser</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>57f74e59-6768-4327-9693-f6a7cbe73fd9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b64d10ef-eb8a-474d-8c0c-0849433fe29e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC_Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>569036af-f424-431a-bbaa-6c03fe1194ba</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Raw Data/Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>569036af-f424-431a-bbaa-6c03fe1194ba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>ab6b6e18-0195-4d02-9312-18e47646bc3c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>569036af-f424-431a-bbaa-6c03fe1194ba</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>04a6148e-14cf-40e9-8667-1d91024303eb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>84b4aa10-078d-43ef-a25b-3f3b7a73cb4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Detail Product/TC_AddToCart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>53394211-3aac-4b50-993a-6cd04d55b7f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Detail Product/TC_RemoveFromCart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>03c9a349-b6df-4b2b-9083-07b3dcaecf55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Close_Browser</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
