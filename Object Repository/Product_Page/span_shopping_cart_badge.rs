<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_shopping_cart_badge</name>
   <tag></tag>
   <elementGuidId>a6bafc5a-5f88-4afe-8f10-dfc0959100ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'shopping_cart_badge']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>shopping_cart_badge</value>
      <webElementGuid>3fc0d2c9-fb3f-4355-946b-979b5968eb00</webElementGuid>
   </webElementProperties>
</WebElementEntity>
