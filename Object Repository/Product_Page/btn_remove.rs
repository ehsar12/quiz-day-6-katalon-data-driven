<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_remove</name>
   <tag></tag>
   <elementGuidId>d24fe0f7-82ef-4da2-a151-e131a1c24abb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'remove-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>remove-sauce-labs-backpack</value>
      <webElementGuid>7a3c9ba2-3cd0-4635-b0eb-58a51ce30d32</webElementGuid>
   </webElementProperties>
</WebElementEntity>
