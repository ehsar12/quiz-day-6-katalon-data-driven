<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_addToCart</name>
   <tag></tag>
   <elementGuidId>32aeec0a-91c7-4ae4-9479-aea77673b6f4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'add-to-cart-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-backpack</value>
      <webElementGuid>d48eaa42-bc07-4288-a1a0-ba4a4734fb5e</webElementGuid>
   </webElementProperties>
</WebElementEntity>
